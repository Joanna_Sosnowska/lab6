﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        public static string[] authors = new string[] { "Joanna Sosnowska" };
        public static string group = "ISI";

        #region P1

        public static Func<IContainer> ContainerFactory = () => new Container.Implementation.Container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(Lab5.ControlPanel.Implementation.Adapter));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Lab5.MainComponent.Contract.ICalculator));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Lab5.MainComponent.Implementation.Calculator));

        #endregion
    }
}
