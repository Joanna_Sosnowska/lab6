﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using System.Runtime.CompilerServices;


namespace Lab6.Test
{
    class Helpers
    {
        internal static void Should_Contains_Methods(Type t)
        {
            // Arrange
            var methods = t.GetMethods();

            // Assert
            Assert.That(methods, Is.Not.Empty);
        }

        internal static void Should_Contains_At_Least_Methods(Type t, int count)
        {
            // Arrange
            var methods = t.GetMethods();

            // Assert
            Assert.That(methods, Has.Length.GreaterThanOrEqualTo(count));
        }

        internal static void Should_Contains_Method(Type t, string methodName)
        {
            // Arrange
            var method = t.GetMethod(methodName);

            // Assert
            Assert.That(method, Is.Not.Null);
        }


        internal static void Should_Inherits_From(Type y, Type x)
        {
            // Arrange
            var interfaces = y.GetInterfaces();

            // Assert
            Assert.That(interfaces, Has.Member(x));
        }

        internal static void Should_Implement_Method(Type x, string methodName, object[] parameters)
        {
            // Arrange
            var instance = Activator.CreateInstance(x);
            var method = x.GetMethod(methodName);

            // Act
            method.Invoke(instance, parameters);
        }

        internal static void Should_Have_Number_Of_Methods_Between(Type type, int low, int up)
        {
            // Arrange
            var methods = type.GetMethods();

            // Assert
            Assert.That(methods, Has.Length.AtLeast(low).And.Length.AtMost(up));
        }

        internal static void Should_Have_An_Extension_Method_For(Type x, Type y)
        {
            // Arrange
            var methods  = from method in x.GetMethods(BindingFlags.Static
                            | BindingFlags.Public | BindingFlags.NonPublic)
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == y
                        select method;

            // Assert
            Assert.That(methods, Is.Not.Empty);
        }

        internal static IEnumerable<MethodInfo> GetExtensionMethods(Assembly assembly, Type extendedType)
        {
            var query = from type in assembly.GetTypes()
                        where type.IsSealed && !type.IsGenericType && !type.IsNested
                        from method in type.GetMethods(BindingFlags.Static
                            | BindingFlags.Public | BindingFlags.NonPublic)
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == extendedType
                        select method;
            return query;
        }
    }
}
